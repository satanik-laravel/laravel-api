<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 09.04.18
 * Time: 23:59
 */

namespace Satanik\Api\Tests;

use Tests\TestCase;

class LocalisationTest extends TestCase
{
    protected $use_user = false;

    /**
     * Setup the test environment.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    protected function setUp(): void
    {
        parent::setUp();
        \Config::set('satanik-api.locales', ['en', 'de']);
    }

    public function testRouteWithoutLocalisationSucceeds(): void
    {
        $response = $this->get('test', ['accept' => 'application/json']);

        $response->assertJson([
            'error' => 0,
        ]);
        $response->assertSuccessful();
    }

    public function testRouteWithEnglishLocalisationSucceeds(): void
    {
        $response = $this->get('en/test', ['accept' => 'application/json']);

        $response->assertJson([
            'error' => 0,
        ]);
        $response->assertSuccessful();
    }

    public function testRouteWithGermanLocalisationSucceeds(): void
    {
        $response = $this->get('de/test', ['accept' => 'application/json']);

        $response->assertJson([
            'error' => 0,
        ]);
        $response->assertSuccessful();
    }


    public function testRouteWithFrenchLocalisationFails(): void
    {
        $response = $this->get('fr/test', ['accept' => 'application/json']);
        $response->assertJson([
            'error' => 10027,
        ]);
        $response->assertStatus(404);
    }
}
