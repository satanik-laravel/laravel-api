<?php

namespace Satanik\Api\Exceptions;

use Satanik\Api\Types\Data;
use Satanik\Exceptions\Types\Exception;
use Satanik\Exceptions\Types\ExceptionRenderer as BaseRenderer;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExceptionRenderer extends BaseRenderer
{
    /**
     * @param \Satanik\Exceptions\Types\Exception $e
     *
     * @return null|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function render(Exception $e): ?JsonResponse
    {
        $error = [
            'error'    => $e->getError(),
            'messages' => $e->getMessages(),
        ];

        $data = $e->getData();
        if (\is_string($data)) {
            $data = json_decode($data);
        }
        if ($data === null) {
            $data = $e->getData();
        }
        $data = json_decode(json_encode($data), true);

        $error          = array_merge(
            $error,
            Data::make()
                ->wrap($data)
                ->withTime()
                ->withBenchmarks()
                ->get()
        );
        $error['file']  = $e->getFile() . ':' . $e->getLine();
        $error['trace'] = [];

        $trace = json_decode(json_encode($e->getTrace() ?? [], JSON_PARTIAL_OUTPUT_ON_ERROR));

        $error['trace'] = array_map(function ($item) {
            $file     = (str_replace(base_path(), '', $item->file ?? '{{file}}'));
            $line     = $item->line ?? '{{line}}';
            $class    = $item->class ?? '{{class}}';
            $type     = $item->type ?? '{{operation}}';
            $function = $item->function ?? '{{function}}';
            return str_replace('\\', '/', "$file:$line ($class$type$function)");
        }, $trace);

        $response = response()->json($error, $e->getCode(), [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $response->setContent(preg_replace('/\\\\+/', '/', $response->content()));
        return $response;
    }
}
