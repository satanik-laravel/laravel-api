<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 19.12.17
 * Time: 10:49
 */

if (!function_exists('start_benchmark')) {
    /**
     * Starts a measure
     *
     * @param string $name Internal name, used to stop the measure
     * @param string $label Public name
     */
    function start_benchmark($name, $label = null)
    {
        if (isset(debugbar()['ram'])) {
            debugbar()['ram']->startMeasure($name, $label);
        }
    }
}

if (!function_exists('stop_benchmark')) {
    /**
     * Stop a measure
     *
     * @param string $name Internal name, used to stop the measure
     */
    function stop_benchmark($name)
    {
        if (isset(debugbar()['ram'])) {
            debugbar()['ram']->stopMeasure($name);
        }
    }
}
