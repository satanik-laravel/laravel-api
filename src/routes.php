<?php

if (app('env') == 'testing') {
    Route::group([
        'prefix'     => '{locale?}',
        'middleware' => 'satanik.api.intercept',
    ], function () {
        Route::get('test', function () {
            return response()->json([
                'key'  => 'result',
                'data' => 'success',
            ]);
        });
    });
}
