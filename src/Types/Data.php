<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 09.11.17
 * Time: 21:16
 */

namespace Satanik\Api\Types;

use Carbon\Carbon;

class Data
{
    /** @var array $data */
    private $data;

    private function __construct()
    {
    }

    public static function make(): self
    {
        return new Data;
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function wrap($data): self
    {
        $this->data = [];
        if (\is_array($data) && isset($data['key'], $data['data'])) {
            $this->data = array_diff_key($data, array_flip(['key', 'data']));
        }

        $key  = $data['key'] ?? 'data';
        $data = $data['data'] ?? $data;

        if ($data !== null &&
            (!\is_string($data) || $data !== '') &&
            (!\is_array($data) || count($data) > 0)) {

            $this->data[$key] = $data;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function withTime(): self
    {
        $this->data = array_merge(
            [
                'timezone'     => Carbon::now()->tzName,
                'current time' => Carbon::now()->format('Y-m-d H:i'),
            ],
            $this->data
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function withBenchmarks(): self
    {
        if (app()->bound('debugbar') && debugbar()->isEnabled() && debugbar()->hasCollector('ram')) {
            $measures = debugbar()->getData()['ram']['measures'];
            $measures = array_filter($measures, function ($measure) {
                return !\in_array($measure['label'], ['Booting', 'Application']);
            });

            $mc = new MeasureContainer();
            foreach ($measures as $measure) {
                $mc->add($measure);
            }
            $call_times = $mc->toArray();
            if (\is_array($call_times) && !empty($call_times)) {
                $this->data['benchmarks'] = $mc->toArray();
            }

            if (empty($this->data['benchmarks'])) {
                unset($this->data['benchmarks']);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $data       = $this->data;
        $this->data = null;
        return $data;
    }
}
