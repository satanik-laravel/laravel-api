<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 01.11.17
 * Time: 22:04
 */

namespace Satanik\Api\Types;

use JsonSerializable;

class MeasureList implements JsonSerializable {
    private $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function &iterate()
    {
        foreach ($this->data as &$v) {
            yield $v;
        }
    }

    public function insert(MeasureObject $mo): void
    {
        $this->data[] = $mo;
    }

    public function empty(): bool
    {
        return empty($this->data);
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}

