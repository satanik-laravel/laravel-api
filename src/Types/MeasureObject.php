<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 01.11.17
 * Time: 22:04
 */

namespace Satanik\Api\Types;

use JsonSerializable;

class MeasureObject implements JsonSerializable
{
    private $name;
    private $start;
    private $end;
    private $time;
    private $time_readable;
    private $start_memory;
    private $end_memory;
    private $total_memory;
    private $total_memory_readable;
    public  $children;

    public function __construct(
        $name,
        $start,
        $end,
        $time,
        $time_readable,
        $start_memory,
        $end_memory,
        $total_memory,
        $total_memory_readable
    ) {
        $this->name                  = $name;
        $this->start                 = $start;
        $this->end                   = $start + $time;
        $this->time                  = $time;
        $this->time_readable         = $time_readable;
        $this->start_memory          = $start_memory;
        $this->end_memory            = $end_memory;
        $this->total_memory          = $total_memory;
        $this->total_memory_readable = $total_memory_readable;

        $this->children = new MeasureList();
    }

    public function name()
    {
        return $this->name;
    }

    public function start()
    {
        return $this->start;
    }

    public function end()
    {
        return $this->end;
    }

    public function time()
    {
        return $this->time;
    }

    public function timeReadable()
    {
        return $this->time_readable;
    }

    public function startMemory()
    {
        return $this->start_memory;
    }

    public function endMemory()
    {
        return $this->end_memory;
    }

    public function totalMemory()
    {
        return $this->total_memory;
    }

    public function totalMemoryReadable()
    {
        return $this->total_memory_readable;
    }

    public function inside(MeasureObject $mo): bool
    {
        return $mo->start() >= $this->start && $mo->end() <= $this->end;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        foreach ($vars as $key => $var) {
            if ($var instanceof MeasureList && $var->empty()) {
                unset($vars[$key]);
            }
        }
        return $vars;
    }
}

