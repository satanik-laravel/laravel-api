<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 19.12.17
 * Time: 11:01
 */

namespace Satanik\Api\Types;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DebugBarException;

class MemoryDataCollector extends DataCollector
{
    protected $request_start_time;
    protected $request_start_memory;
    protected $request_end_time;
    protected $request_end_memory;
    protected $started_measures = [];
    protected $measures         = [];

    public function __construct($request_start_time = null)
    {
        if ($request_start_time === null) {
            if (isset($_SERVER['REQUEST_TIME_FLOAT'])) {
                $request_start_time = $_SERVER['REQUEST_TIME_FLOAT'];
            } else {
                $request_start_time = microtime(true);
            }
        }
        $this->request_start_time   = $request_start_time;
        $this->request_start_memory = memory_get_peak_usage(true);
    }

    /**
     * @param string      $name
     * @param string|null $label
     * @param string|null $collector
     */
    public function startMeasure($name, $label = null, $collector = null): void
    {
        $start                         = microtime(true);
        $start_memory                  = memory_get_peak_usage(true);
        $this->started_measures[$name] = array(
            'label'        => $label ?: $name,
            'start'        => $start,
            'start_memory' => $start_memory,
            'collector'    => $collector,
        );
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasStartedMeasure($name): bool
    {
        return isset($this->started_measures[$name]);
    }

    /**
     * @param       $name
     * @param array $params
     *
     * @throws \DebugBar\DebugBarException
     */
    public function stopMeasure($name, array $params = array()): void
    {
        $end        = microtime(true);
        $end_memory = memory_get_peak_usage(true);
        if (!$this->hasStartedMeasure($name)) {
            throw new DebugBarException("Failed stopping measure '$name' because it hasn't been started");
        }
        $this->addMeasure(
            $this->started_measures[$name]['label'],
            $this->started_measures[$name]['start'],
            $end,
            $this->started_measures[$name]['start_memory'],
            $end_memory,
            $params,
            $this->started_measures[$name]['collector']
        );
        unset($this->started_measures[$name]);
    }

    /**
     * @param string      $label
     * @param float       $start
     * @param float       $end
     * @param int         $startMemory
     * @param int         $endMemory
     * @param array       $params
     * @param string|null $collector
     */
    public function addMeasure(
        $label,
        $start,
        $end,
        $startMemory,
        $endMemory,
        array $params = [],
        $collector = null
    ): void {
        $this->measures[] = array(
            'label'                 => $label,
            'start'                 => $start,
            'relative_start'        => $start - $this->request_start_time,
            'end'                   => $end,
            'relative_end'          => $end - $this->request_end_time,
            'start_memory'          => $startMemory,
            'relative_start_memory' => $startMemory - $this->request_start_memory,
            'end_memory'            => $endMemory,
            'relative_end_memory'   => $endMemory - $this->request_start_memory,
            'duration'              => $end - $start,
            'duration_str'          => $this->getDataFormatter()->formatDuration($end - $start),
            'total_memory'          => $endMemory - $startMemory,
            'total_memory_str'      => $this->getDataFormatter()->formatBytes($endMemory - $startMemory),
            'params'                => $params,
            'collector'             => $collector,
        );
    }

    /**
     * @param string      $label
     * @param \Closure    $closure
     * @param string|null $collector
     *
     * @throws \DebugBar\DebugBarException
     */
    public function measure($label, \Closure $closure, $collector = null): void
    {
        $name = spl_object_hash($closure);
        $this->startMeasure($name, $label, $collector);
        $result = $closure();
        $params = \is_array($result) ? $result : array();
        $this->stopMeasure($name, $params);
    }

    /**
     * @return array
     */
    public function getMeasures(): array
    {
        return $this->measures;
    }

    /**
     * @return int|null
     */
    public function getRequestDuration(): ?int
    {
        if ($this->request_end_time !== null) {
            return $this->request_end_time - $this->request_start_time;
        }
        return microtime(true) - $this->request_start_time;
    }

    /**
     * @return int|null
     */
    public function getRequestTotalMemory(): ?int
    {
        if ($this->request_end_memory !== null) {
            return $this->request_end_memory - $this->request_start_memory;
        }
        return memory_get_peak_usage(true) - $this->request_start_memory;
    }

    /**
     * @return array
     * @throws \DebugBar\DebugBarException
     */
    public function collect(): array
    {
        $this->request_end_time   = microtime(true);
        $this->request_end_memory = memory_get_peak_usage(true);
        foreach (array_keys($this->started_measures) as $name) {
            $this->stopMeasure($name);
        }

        usort($this->measures, function ($a, $b) {
            if ($a['start'] === $b['start']) {
                return 0;
            }
            return $a['start'] < $b['start'] ? -1 : 1;
        });

        return array(
            'start'            => $this->request_start_time,
            'end'              => $this->request_end_time,
            'duration'         => $this->getRequestDuration(),
            'duration_str'     => $this->getDataFormatter()->formatDuration($this->getRequestDuration()),
            'total_memory'     => $this->getRequestTotalMemory(),
            'total_memory_str' => $this->getDataFormatter()->formatBytes($this->getRequestTotalMemory()),
            'measures'         => array_values($this->measures),
        );
    }

    public function getName(): string
    {
        return 'ram';
    }

}
