<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 01.11.17
 * Time: 22:04
 */

namespace Satanik\Api\Types;

class MeasureContainer
{
    /** @var MeasureObject $data */
    private $data;

    public function __construct()
    {
        $this->data = new MeasureList();
    }

    public function add($obj): void
    {
        $mo      = new MeasureObject(
            $obj['label'],
            $obj['relative_start'],
            $obj['relative_end'],
            $obj['duration'],
            $obj['duration_str'],
            $obj['relative_start_memory'],
            $obj['relative_end_memory'],
            $obj['total_memory'],
            $obj['total_memory_str']);
        $stack   = [];
        $stack[] =& $this->data;
        while (!empty($stack)) {
            /** @var MeasureList $current */
            $current = array_pop($stack);
            foreach ($current->iterate() as &$measure) {
                if ($measure->inside($mo)) {
                    $stack[] =& $measure->children;
                    $current = null;
                    break;
                }
            }
            unset($measure);
            if ($current !== null && $mo !== null) {
                $current->insert($mo);
                $mo = null;
            }
        }
    }

    public function toArray()
    {
        return json_decode(json_encode($this->data), true);
    }
}

