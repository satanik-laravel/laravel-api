<?php

namespace Satanik\Api;

use App;
use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Route;
use Satanik\Api\Exceptions\ExceptionRenderer;
use Satanik\Api\Middleware\Intercept;
use Satanik\Api\Types\MemoryDataCollector;
use Satanik\Exceptions\Types\ExceptionRenderer as BaseRenderer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use URL;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router): void
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $router->aliasMiddleware('satanik.api.intercept', Intercept::class);

        $this->setAppLocale();

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/satanik-api.php', 'satanik-api');

        $this->registerHelpers();
        $this->extendDebugbar();

        $this->app->singleton(BaseRenderer::class, ExceptionRenderer::class);
    }

    private function setAppLocale(): void
    {
        Route::matched(function (RouteMatched $event) {
            // Get language from route.
            $locale = $event->route->parameter('locale');
            $event->route->forgetParameter('locale');

            if (empty($locale)) {
                $locale = config('app.locale');
            } elseif (!\in_array($locale, config('satanik-api.locales', []), false)) {
                throw new NotFoundHttpException();
            }

            // Ensure, that all built urls would have "_locale" parameter set from url.
            Url::defaults(['locale' => $locale]);

            // Change application locale.
            App::setLocale($locale);
        });
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/satanik-api.php' => config_path('satanik-api.php'),
        ], 'satanik.api.config');
    }

    private function registerHelpers(): void
    {
        foreach (glob(__DIR__ . '/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

    private function extendDebugbar(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        debugbar()->addCollector(new MemoryDataCollector());
    }
}
