<?php

namespace Satanik\Api\Concerns;

use Exception;
use Illuminate\Http\Request;
use Route;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait RendersLocalised
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $exception
     *
     * @return null|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    protected function renderLocalised(Request $request, Exception $exception)
    {
        // When we've got non-matched route resulting in "404 Not Found" or "405 Not Allowed" response.
        if ($exception instanceof NotFoundHttpException ||
            $exception instanceof MethodNotAllowedHttpException) {
            $routes = Route::getRoutes()->get($request->method());
            $uri    = null;
            shuffle($routes);
            $result         = array_first($routes, function (\Illuminate\Routing\Route $route) use ($request, &$uri) {
                if (preg_match('/{locale[?]?}/', $route->uri)) {
                    $url     = '/' . $route->uri;
                    $pattern = preg_replace_callback(
                        "#{(?:\w|-)*(?'questionmark'[?])?}(?'slash'(?(questionmark)/|))#",
                        function ($matches) {
                            return "(?:((?:\\w|\\d|-)+){$matches['slash']}){$matches['questionmark']}";
                        },
                        $url);

                    $parameters = null;
                    if (preg_match("#$pattern#", $request->getRequestUri(), $parameters)) {
                        $parameters = \array_slice($parameters, 1);
                        $url        = preg_replace_array('#{(?:\w|-)*([?])?}(?(1)/|)#', $parameters, $url);

                        $counter = 0;
                        $uri     = '/' . preg_replace_callback(
                                '/{(?:\w|-)*[?]?}/',
                                function ($matches) use ($parameters, &$counter) {
                                    if (preg_match('/{locale[?]?}/', $matches[0])) {
                                        $replace = $matches[0];
                                    } else {
                                        $replace = $parameters[$counter];
                                    }
                                    $counter++;
                                    return $replace;
                                }, $route->uri);
                    }

                    return ($url === $request->getPathInfo()) &&
                        \in_array($request->method(), $route->methods(), false);
                }
                return false;
            });
            $default_locale = \App::getLocale() ?? config('app.locale');
            if (!empty($result) && !empty($uri)) {
                /** @var \Illuminate\Routing\Route $localisable */
                $new_request             = Request::create(
                    preg_replace('/{locale[?]?}/', $default_locale, $uri),
                    $request->method()
                );
                $new_request->headers    = clone $request->headers;
                $new_request->query      = clone $request->query;
                $new_request->request    = clone $request->request;
                $new_request->attributes = clone $request->attributes;
                $new_request->files      = clone $request->files;
                $new_request->cookies    = clone $request->cookies;
                return app()->handle($new_request);
            }
        }

        return null;
    }
}
