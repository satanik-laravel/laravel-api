<?php

namespace Satanik\Api\Middleware;

use App;
use Closure;
use Illuminate\Http\Response;
use Satanik\Api\Types\Data;

class Intercept
{
    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        start_benchmark('call');

        $request->headers->set('Accept', 'application/json');

        /** @var Response $response */
        $response = $next($request);

        if ($response->status() !== 200) {
            return $response;
        }

        stop_benchmark('call');

        $json_response = [
            'error' => 0,
        ];

        $data = $response->content();
        if (\is_string($data)) {
            $data = json_decode($data);
        }
        if ($data === null) {
            $data = $response->content();
        }
        $data = json_decode(json_encode($data), true);

        $json_response = array_merge(
            $json_response,
            Data::make()
                ->wrap($data)
                ->withTime()
                ->withBenchmarks()
                ->get()
        );

        return response()->json($json_response, 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
