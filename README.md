# Satanik API Package

[![Software License][ico-license]](LICENSE.md)

This package primarly adds the functionality to access routes localised. This means that by accessing the route with a `locale` parameter, the locale of the app will be changed as well. Furthermore a middleware and a specialised exception renderer standardise json responses.

### Dependencies

* satanik/exceptions - [git@gitlab.com:satanik/laravel-api.git]()

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/api
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/api": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-api.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/api": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/Api
```

and use this code in the composer file

```json
"repositories": {
  "satanik/api": {
    "type": "path",
    "url": "packages/Satanik/Api",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/api": "@dev",
```

afterwards publish the configuration

```bash
$ php artisan vendor:publish --tag=satanik.api.config
```

## Usage

### Localisation for routes

To add localisation first add the languages available to `config/satanik.api.php`
```php
'locales' => ['en','fr']
```
Furthermore the `ExceptionHandler` must be decorated to handle `404` and `405` and lookup the routes in other languages. For this make the following changes to the `App\Exceptions\Handler` class

```php
namespace App\Exceptions;

use Exception;
use Satanik\Api\Concerns\RendersLocalised; // add trait
use Satanik\Exceptions\Types\ExceptionHandler as BaseHandler; // add base class

class Handler extends BaseHandler // inherit base class
{
    use RendersLocalised; // use trait
    
    ...
    
    public function render($request, Exception $exception)
    {
        // use trait class to recreate requests after one language was not found
        if ($response = $this->renderLocalised($request, $exception)) {
            return $response;
        }
        
        return parent::render($request, $exception);
    }
}
```

Then the `locale` variable can be used for all the routes

```php
Route::get('{locale?}/home', function() { ... });
```

### Benchmarking for APIs

This package uses `barryvdh/laravel-debugbar` to benchmark. For a more granular benchmarking mark the beginning and end of scopes to benchmark with the helper function to start

```php
start_benchmark('unique_name');
```

and the helper function to end a scope

```php
stop_benchmark('unique_name');
```



## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
